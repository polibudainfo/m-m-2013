'use strict';

describe('Controller: ParticipantCtrl', function () {

  // load the controller's module
  beforeEach(module('misski2013PageApp'));

  var ParticipantCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParticipantCtrl = $controller('ParticipantCtrl', {
      $scope: scope
    });
  }));

  it('should get contest object from contest cache', function () {});
  it('should get participant object from contest cache', function () {});
});
