'use strict';

describe('Controller: ContestCtrl', function () {

    // load the controller's module
    beforeEach(module('misski2013PageApp'));

    var ParticipantCtrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ParticipantCtrl = $controller('ContestCtrl', {
            $scope: scope
        });
    }));

    it('should get contest out of ContestCache based on contest slug from $routeParams', function () {});
});
