'use strict';

describe('Directive: candidateList', function () {

    // load the directive's module
    beforeEach(module('candidateList'));

    var element,
        scope;

    beforeEach(inject(function ($rootScope) {
        scope = $rootScope.$new();
    }));

    it('should make hidden element visible', inject(function ($compile) {
        element = angular.element('<candidate-list></candidate-list>');
        element = $compile(element)(scope);
        //expect(element.text()).toBe('');
    }));

    it('should wait for ($rootscope app:ready event / $rootScope.appReady to be true) before calling init()',
        function () {
        }
    );

    it('should be able to convert contest type to slug', function () {
    });

    it('should get contest from ContestCache by slug', function () {
    });
});
