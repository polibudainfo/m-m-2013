'use strict';

describe('Service: ContestCache', function () {

    // load the service's module
    beforeEach(module('ContestCache'));

    // instantiate service
    var ContestCache, console
    beforeEach(inject(function (_ContestCache_) {
        ContestCache = _ContestCache_;
        console = {
            warn: function () {
            },
        }
    }));

    it('should return a promise when called a method fetch', function () {
    });

    it('should rise a console warning when called with settings size equal to cache size', function () {
        // TODO: inject proper Settings mock and make cache accessible in tests
        //var warning = spyOn(console, 'warn');
        //expect(warning).toHaveBeenCalledWith('ContestCache.fetch called again. Possibly a bug');
    });

    it('should request 2 contests when called with settings size not equal to cache size', function () {
        // TODO: inject proper Settings mock with 2 contests
    });

    it('should return a promise then asked for contest form cache', function () {
    });

    it('should wait for app:ready if $rootScope.appReady is false before resolving referred when asked for contest',
        function () {
        }
    );

    it('should return a promise then asked for entire cache', function () {
    });

    it('should wait for app:ready if $rootScope.appReady is false before resolving referred when asked for list',
        function () {
        }
    );

});