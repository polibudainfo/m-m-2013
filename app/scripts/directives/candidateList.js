(function () {
    'use strict';

    angular.module('candidateList', ['ContestCache'])
        .directive('candidateList', [
            '$rootScope',
            'ContestCache',
            'Settings',
            function ($rootScope, ContestCache, Settings) {
                return {
                    templateUrl: 'views/directives/candidateList.html',
                    restrict: 'E',
                    scope: {
                        contest: '=contest' // 2-way data-binding made by '='
                    },
                    link: function postLink(scope, element, attrs) {
                        function init() {
                            var slug = !scope.contest ? Settings.contest[attrs.type] : scope.contest.slug;
                            ContestCache.get(slug).then(function (data) {
                                scope.contest = data;
                            });
                            // TODO: move contest type to API
                            scope.type = attrs.type === 'miss' ? 'kandydatek' : 'kandydatów';
                        }

                        // TODO: refactor this into some kind of `initWhenReady` function
                        if ($rootScope.appReady) {
                            init();
                        } else {
                            $rootScope.$on('app:ready', function () {
                                init();
                            })
                        }
                    }
                };
            }
        ]);
})();


