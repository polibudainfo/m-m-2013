(function () {
    'use strict';

    angular.module('misski2013PageApp')
        .controller('ParticipantCtrl', [
            '$scope',
            '$routeParams',
            'ContestCache',
            function ($scope, $routeParams, ContestCache) {
                ContestCache.get($routeParams.contest).then(function (contest) {
                    $scope.contest = contest;
                    $scope.participant = _.find($scope.contest, { 'slug': $routeParams.participant});
                });
            }
        ]);
})();
