(function () {
    'use strict';

    angular.module('misski2013PageApp')
        .controller('MainCtrl', [
            '$scope',
            'ContestCache',
            function ($scope, ContestCache) {
                ContestCache.list().then(function(data) {
                    $scope.contests = data;
                })
            }
        ]);
})();