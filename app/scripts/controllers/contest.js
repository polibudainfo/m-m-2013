(function () {
    'use strict';

    angular.module('misski2013PageApp')
        .controller('ContestCtrl', [
            '$scope',
            '$routeParams',
            'ContestCache',
            function ($scope, $routeParams, ContestCache) {
                ContestCache.get($routeParams.contest).then(function (contest) {
                    $scope.contest = contest;
                });
            }
        ]);
})();