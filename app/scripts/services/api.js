(function () {
    'use strict';

    angular.module('API', ['LocalSettings'])
        .service('PhotoContestAPI', [
            '$http',
            'Settings',
            function PhotoContestAPI($http, Settings) {
                /**
                 * Fetches contest object from API
                 *
                 * @param slug
                 * @returns promise
                 */
                this.getContestBySlug = function getContestBySlug(slug) {
                    return $http.get(Settings.address + '/' + Settings.endpoint.contests + '/' + slug);
                };

                /**
                 * Shorthand for fetching contest object by it's type
                 *
                 * @param type
                 * @returns promise
                 */
                this.getContestByType = function getContestByType(type) {
                    return this.getContestBySlug(Settings.contest[type]);
                };
            }
        ]);
})();
