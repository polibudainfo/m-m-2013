(function () {
    'use strict';

    angular.module('ContestCache', ['API'])
        .service('ContestCache', [
            '$q',
            '$rootScope',
            'PhotoContestAPI',
            'Settings',
            function ($q, $rootScope, PhotoContestAPI, Settings) {

                /**
                 * Holds contest objects fetched form API.
                 */
                var cache = [];

                /**
                 * Fetches all contests into cache. Should be called only once in application's run()
                 */
                this.fetch = function fetch() {
                    var promises = [];
                    // Fetch only if a number of cache entries is not equal number of contests set in settings object.
                    // Prevents re-running fetch function
                    if (_.size(Settings.contest) !== cache.length) {
                        $.each(Settings.contest, function (index, value) {
                            promises.push(PhotoContestAPI.getContestBySlug(value)
                                .then(function (response) {
                                    cache.push(response.data);
                                })
                                .catch(function () {
                                    console.warn('Could not fetch ');
                                }));
                        });
                    } else {
                        console.warn('ContestCache.fetch called again. Possibly a bug');
                    }

                    return $q.all(promises);
                };

                /**
                 * Contest getter
                 *
                 * @param slug
                 * @returns promise
                 */
                this.get = function get(slug) {
                    var deferred = $q.defer();

                    if ($rootScope.appReady) {
                        deferred.resolve(_.find(cache, { 'slug': slug }));
                    } else {
                        $rootScope.$on('app:ready', function () {
                            deferred.resolve(_.find(cache, { 'slug': slug }));
                        });
                    }
                    return deferred.promise;
                }

                /**
                 * Contest cache getter
                 *
                 * @returns promise
                 */
                this.list = function list() {
                    var deferred = $q.defer();

                    if ($rootScope.appReady) {
                        deferred.resolve(cache);
                    } else {
                        $rootScope.$on('app:ready', function () {
                            deferred.resolve(cache);
                        });
                    }
                    return deferred.promise;
                }
            }
        ]);
})();
