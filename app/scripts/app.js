(function () {
    'use strict';

    angular.module('misski2013PageApp', [
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'candidateList',
            'ContestCache',
        ])
        .config([
            '$routeProvider',
            '$locationProvider',
            function ($RouteProvider, $locationProvider) {
                //$locationProvider.html5Mode(true);

                $RouteProvider
                    .when('/', {
                        templateUrl: 'views/main.html',
                        controller: 'MainCtrl'
                    })
                    .when('/contest/:contest', {
                        templateUrl: 'views/contest.html',
                        controller: 'ContestCtrl'
                    })
                    .when('/contest/:contest/participant/:participant', {
                        templateUrl: 'views/participant.html',
                        controller: 'ParticipantCtrl'
                    })
                    .otherwise({
                        redirectTo: '/'
                    });
            }
        ])
        .run([
            '$rootScope',
            'ContestCache',
            function ($rootScope, ContestCache) {
                console.time("fetching data");
                // fetch all contests set in api.Settings
                ContestCache.fetch().then(function () {
                    console.timeEnd('fetching data');
                    $rootScope.$broadcast('app:ready');
                    $rootScope.appReady = true;
                });
            }
        ]);
})();
